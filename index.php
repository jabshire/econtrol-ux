<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>eControl UX</title>
	
	<link href="/lib/css/bootstrap.css" rel="stylesheet">
	<link href="/lib/css/econtrol.css" rel="stylesheet">
	
	<script src="/lib/js/jquery.min.js"></script>
	<script src="/lib/js/bootstrap.min.js"></script>
</head>
<body>

<? include('lib/inc/header.php') ?>

<div class="page">
	<div class="page-side">
		<? include('lib/inc/sidebar-pages.php') ?>
	</div>
	<div class="page-main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					
					<div class="container-fluid">
						
						<h1>List of People</h1>
						
						<div class="collapse navbar-collapse people-list-filter">
							<!--search-->
							<div class="row">
								<div class="col-xs-4">
									<input type="text" class="form-control col-xs-4" placeholder="Search" id="searchPeople">
								</div>
							</div>
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>