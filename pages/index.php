<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>eControl UX</title>
	
	<link href="/lib/css/bootstrap.css" rel="stylesheet">
	<link href="/lib/css/econtrol.css" rel="stylesheet">
	
	<script src="/lib/js/jquery.min.js"></script>
	<script src="/lib/js/bootstrap.min.js"></script>
</head>
<body>

<? include('../lib/inc/header.php') ?>

<div class="page">
	<div class="page-side">
		<? include('../lib/inc/sidebar-content.php') ?>
	</div>
	<div class="page-main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					
					<div class="container-fluid">
						
						<h1>List of Pages</h1>
						
						<div class="people-list-filter">
							<!--search-->
							<div class="row">
								<div class="col-xs-8 col-sm-9 col-md-9 col-lg-10">
									<input type="text" class="form-control" placeholder="Filter" id="searchPeople">
								</div>
								<div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
									<a href="/people/add" class="btn btn-primary btn-block">New</a>
								</div>
							</div>
						</div>
						
						<hr>
						
						<div class="table-responsive people-list" id="peopleList">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>ID</th>
										<th>Title</th>
										<th>Slug</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td><a href="/pages/edit/1"><b>Home Page</b></a></td>
										<td><a href="/">home</a></td>
										<td>Published</td>
									</tr>
									<tr>
										<td>2</td>
										<td><a href="/pages/edit/1"><b>About Us</b></a></td>
										<td><a href="/about/about-us">/about/about-us</a></td>
										<td>Draft</td>
									</tr>
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>