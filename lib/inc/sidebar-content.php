
<ul class="sidebar-nav">
	<li class="active"><a href="/pages"><span class="glyphicon glyphicon-file"></span> Pages</a></li>
	<li><a href="/routes"><span class="glyphicon glyphicon-link"></span> Routes</a></li>
	<li><a href="/menus"><span class="glyphicon glyphicon-align-justify"></span> Menus</a></li>
	<li><a href="/widgets"><span class="glyphicon glyphicon-magnet"></span> Widgets</a></li>
	<li><a href="/forms"><span class="glyphicon glyphicon-list-alt"></span> Forms</a></li>
	<li><a href="/slides"><span class="glyphicon glyphicon-film"></span> Sliders</a></li>
	<li><a href="/media"><span class="glyphicon glyphicon-picture"></span> Media</a></li>
</ul>
