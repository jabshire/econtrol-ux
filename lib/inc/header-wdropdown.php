
<!--header-->
<header class="header">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
	
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">eControl</a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav navbar-main">
					<li class="dropdown">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-folder-open"></span> Content <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="/pages"><span class="glyphicon glyphicon-user"></span> Users</a></li>
							<li><a href="/pages"><span class="glyphicon glyphicon-file"></span> Pages</a></li>
							<li><a href="/routes"><span class="glyphicon glyphicon-link"></span> Routes</a></li>
							<li><a href="/menus"><span class="glyphicon glyphicon-align-justify"></span> Menus</a></li>
							<li><a href="/widgets"><span class="glyphicon glyphicon-magnet"></span> Widgets</a></li>
							<li><a href="/forms"><span class="glyphicon glyphicon-list-alt"></span> Forms</a></li>
							<li><a href="/slides"><span class="glyphicon glyphicon-film"></span> Sliders</a></li>
							<li><a href="/media"><span class="glyphicon glyphicon-picture"></span> Media</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-wrench"></span> Configurations <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="/admin/modules"><span class="glyphicon glyphicon-bell"></span> Modules</a></li>
							<li><a href="/admin/notifications"><span class="glyphicon glyphicon-envelope"></span> Notifications</a></li>
						</ul>
					</li>
				</ul>
		
				<ul class="nav navbar-nav navbar-right navbar-account">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-off"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#"><span class="glyphicon glyphicon-globe"></span> Site</a></li>
							<li class="divider"></li>
							<li><a href="/logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
						</ul>
					</li>
				</ul>
			
			</div><!-- /.navbar-collapse -->
			
		</div><!-- /.container-fluid -->
	</nav>
</header>
<!--/header-->
