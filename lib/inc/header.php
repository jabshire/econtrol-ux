
<!--header-->
<header class="header">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
	
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">eControl</a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav navbar-main">
					<li class="active">
						<a href="/pages"><span class="glyphicon glyphicon-folder-open"></span> Content</a>
					</li>
					<li>
						<a href="/config"><span class="glyphicon glyphicon-wrench"></span> Configurations</a>
					</li>
					<li>
						<a href="/users"><span class="glyphicon glyphicon-user"></span> Users</a>
					</li>
				</ul>
		
				<ul class="nav navbar-nav navbar-right navbar-account">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-off"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#"><span class="glyphicon glyphicon-globe"></span> Site</a></li>
							<li class="divider"></li>
							<li><a href="/logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
						</ul>
					</li>
				</ul>
			
			</div><!-- /.navbar-collapse -->
			
		</div><!-- /.container-fluid -->
	</nav>
</header>
<!--/header-->
